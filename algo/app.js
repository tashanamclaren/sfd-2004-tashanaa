// palidrome
// create a function that returns true if string is a palidrome and return false if it isn't.
// palidrome is a word that is spelled backwards and forwards the same way..
function isPali(word) {
    // let splitString = word.split('');
    // let reverseArray = splitString.reverse();
    // let reversedWord = reverseArray.join('');
    // return reversedWord === word;
    return word.split('').reverse().join('') === word;
};

console.log(isPali('racecar')); // print true
console.log(isPali('joshua')); // print false

// anagram - two words that are spelled the same with the letters used.
// create a function that return true if the words are an anagram and return false if they are not anagrams.
function isAnagram(word1, word2) {
    // let one = word1;
    // let two = word2;
    // let oneArr = one.split('');
    // let twoArr = two.split('');
    // let oneSortedArr = oneArr.sort();
    // let twoSortedArr = twoArr.sort();
    // let oneSortedString = oneSortedArr.join('');
    // let twoSortedString = twoSortedArr.join('');
    return word1.split('').sort().join('') === word2.split('').sort().join('');
};
console.log(isAnagram('mary', 'army')); // return true
console.log(isAnagram('joshua', 'pizza')); // return false

/* 
    Write a program that prints the numbers from 1 to 100. But for multiples of three print "Fizz" 
    instead of the number and for the multiples of five print "Buzz". For numbers which are multiples of 
    both three and five print "FizzBuzz"
    1
    2
    3 - Fizz
    4
    5 - Buzz
    ...
    15 - FizzBuzz
*/

function fizzBuzz() {
    for(i=0;i<100;i++){
        if (i % 3 === 0 && i % 5 === 0) {
            console.log(`fizzbuzz - ${i}`);
        } else if (i % 3 === 0) {
            console.log(`fizz - ${i}`);
        } else if (i % 5 === 0) {
            console.log(`buzz - ${i}`);
        } else {
            console.log(i);
        }
    }
};

fizzBuzz();