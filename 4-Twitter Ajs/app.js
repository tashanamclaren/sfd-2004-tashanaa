var app = angular.module("twitterApp", []);

app.controller("firstController", function ($scope, $http) {
  $scope.greeting = "Welcome to Twitter";
  $scope.tweetContent = "";
  $scope.isCollapsed = true;
  $scope.username = "";
  $scope.startingCollapse = false;
  $scope.collapsed2 = true;
  $scope.tweets = [];
  $scope.guidGenerator = function () {
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (
      c
    ) {
      var r = (Math.random() * 16) | 0,
        v = c == "x" ? r : (r & 0x3) | 0x8;
      return v.toString(16);
    });
  };

  $scope.onLogin = function () {
    $http
      .get("https://twitter-clone-94250.firebaseio.com/Users/.json")
      .then((response) => {
        const users = response.data;
        if (users.includes($scope.username)) {
          sessionStorage.setItem("user", $scope.username);
          this.isCollapsed = !this.isCollapsed;
          this.startingCollapse = !this.startingCollapse;
          this.collapsed2 = !this.collapsed2;
          $http
            .get("https://twitter-clone-94250.firebaseio.com/tweets/.json")
            .then((tweets) => {
              $scope.tweets = tweets.data;
            });
        } else {
          alert("user does not exist, please try again!");
        }
      })
      .catch((err) => {
        alert("error fetching data from url" + err.config.url);
        console.log(err);
      });
  };

  $scope.createTweet = function () {
    //console.log("create Tweet");
    const idGuid = $scope.guidGenerator();
    console.log($scope.tweetContent);
    const newPostData = {
      tweetContent: $scope.tweetContent,
      id: idGuid,
      date: Date.now(),
      user: sessionStorage.getItem("user"),
    };
    const url = `https://twitter-clone-94250.firebaseio.com/tweets/${idGuid}/.json`;
    $http.patch(url, newPostData).then((data) => {
      $scope.tweets[data.data.id] = data.data;
      $scope.tweetContent = "";
    });
  };

  $scope.editTweet = function () {
    console.log("User updated their tweet");
  };

  $scope.deleteTweet = function (id) {
    let data = {
      tweetContent: tweetContent,
      id: idGuid,
      user: user,
      date: Date.now(),
    };
    $http
      .delete(
        `https://twitter-clone-94250.firebaseio.com/tweets/${idGuid}/.json`,
        {
          method: "delete",
        }
      )
      .then(() => {
        console.log("tweet deleted");
      });
  };
});
